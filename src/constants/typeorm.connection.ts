import {
  DB_USER,
  DB_PASSWORD,
  DB_HOST,
  DB_NAME,
} from '../config/config.service';
import { MongoConnectionOptions } from 'typeorm/driver/mongodb/MongoConnectionOptions';
import { ServerSettings } from '../system-settings/entities/server-settings/server-settings.entity';
import { TokenCache } from '../auth/entities/token-cache/token-cache.entity';

export function connectTypeorm(config): MongoConnectionOptions {
  return {
    url: `mongodb://${config.get(DB_USER)}:${config.get(
      DB_PASSWORD,
    )}@${config.get(DB_HOST)}/${config.get(DB_NAME)}?useUnifiedTopology=true`,
    type: 'mongodb',
    logging: false,
    synchronize: true,
    entities: [ServerSettings, TokenCache],
    useNewUrlParser: true,
  };
}
